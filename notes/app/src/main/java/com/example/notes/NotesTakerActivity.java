package com.example.notes;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.notes.Models.Notes;

import java.text.SimpleDateFormat;
import java.util.Date;

public class NotesTakerActivity extends AppCompatActivity {

    EditText editText_title,editText_notes;
    ImageView imageView_save;
    Notes notes;
    boolean anOldNote = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes_taker);

        //on gere l'enregistrement lors du click sur le bouton save
        //on récupere le titre et la description saisi pour les envoyé à notre activité principale
        //pour l'ajouter dans la BD

        imageView_save = findViewById(R.id.imageView_save);
        editText_title = findViewById(R.id.editText_title);
        editText_notes = findViewById(R.id.editText_notes);

        notes = new Notes();
        try{
            notes = (Notes) getIntent().getSerializableExtra("old_version_note");
            editText_title.setText(notes.getTitle());
            editText_notes.setText(notes.getNotes());
            anOldNote = true;
        }
        catch (Exception e){
            e.printStackTrace();
        }


        imageView_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = editText_title.getText().toString();
                String description = editText_notes.getText().toString();

                if(description.isEmpty()){
                    Toast.makeText(NotesTakerActivity.this,"Veuillez ajouter une note!", Toast.LENGTH_SHORT).show();
                    return;
                }
                //EEE = jour de la semain d = date MMM= mois yyy = année
                SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyy HH:mm");
                Date date  = new Date();

                if(!anOldNote){
                    notes = new Notes();
                }


                notes.setTitle(title);
                notes.setNotes(description);
                notes.setDate(dateFormat.format(date));

                //on envoie les données à notre activité principale
                Intent intent = new Intent();
                intent.putExtra("note",notes);

                setResult(Activity.RESULT_OK, intent);
                finish();

            }
        });
    }
}