package com.example.notes.Database;

//Dao, cette interfece nous permet d'acceder aux données


import static androidx.room.OnConflictStrategy.REPLACE;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.notes.Models.Notes;

import java.util.List;

@Dao
public interface MainDAO {

    @Insert(onConflict = REPLACE)
    void insert(Notes notes);


    // ORDER BY id DESC: pour afficher les derniers elements ajoutés en haut de notre page
    @Query("SELECT * FROM notes ORDER BY id DESC")
    List<Notes> getAll();


    @Query("UPDATE notes SET title = :title, notes = :notes WHERE Id = :id")
    void update(int id, String title, String notes);


    @Delete
    void delete(Notes notes);



}
