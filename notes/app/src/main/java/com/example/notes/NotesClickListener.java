package com.example.notes;

import androidx.cardview.widget.CardView;

import com.example.notes.Models.Notes;

public interface NotesClickListener {

    void onClick(Notes notes);
    //on passe l'objet de la vue principale en parametre aussi car c'est le conteneur des elements
    void onLongClick(Notes notes, CardView cardView);



}
