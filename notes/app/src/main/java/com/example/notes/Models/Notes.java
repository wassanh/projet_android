package com.example.notes.Models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;


@Entity(tableName = "notes") //pour indiquer que c'est une intité de notre BD avec le nom de la teble en argument
public class Notes implements Serializable {

    @PrimaryKey(autoGenerate = true)
    int Id = 0;


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    @ColumnInfo(name = "title")
    String title = "";


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @ColumnInfo(name = "notes")
    String notes = "";


    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }


    @ColumnInfo(name = "date")
    String date = "";


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    @ColumnInfo(name = "pinned")
    boolean pinned = false;


    public boolean isPinned() {
        return pinned;
    }

    public void setPinned(boolean pinned) {
        this.pinned = pinned;
    }





}
