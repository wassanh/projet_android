package com.example.notes;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.SearchView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.example.notes.Adapter.NotesListAdapter;
import com.example.notes.Database.RoomDB;
import com.example.notes.Models.Notes;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener{
    //création des objets pour le recyclerView et l'adaptateur
    RecyclerView recyclerView;
    NotesListAdapter notesListAdapter;
    List<Notes> notes = new ArrayList<>();
    RoomDB database;
    FloatingActionButton fab_add;
    SearchView searchView_home;
    Notes choosenNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //on lie les objets avec les id qui leur correspond dans l'activité principale
        //initialisation des variabels
        recyclerView = findViewById(R.id.recycler_home);
        fab_add = findViewById(R.id.fab_add);
        searchView_home = findViewById(R.id.searchView_home);

        database = RoomDB.getInstance(this);
        notes = database.mainDAO().getAll();

        //on met à jour ces données dans le recyclerView
        updateRecycler(notes);

        //bouton qui gere l'ajout des informations dans la base de données en commençant une nouvelle activité
        //dans laquelle on rentre le titre et la description apres on l'enregistre dans la BD

        fab_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,NotesTakerActivity.class);
                //on appelle l'activité de demarrage pour le résultat avec l'intention qu'on vient de créer et le code de demande d'accord
                //c'est pour pouvoir ajouter les information
                startActivityForResult(intent,101);

            }
        });

        //définir les fonctions pour la barre de recherche
        searchView_home.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                filter(newText);
                return true;
            }
        });
    }

        private void filter(String newText) {
            List<Notes> filter = new ArrayList<>();
            for (Notes note:notes){
                if(note.getTitle().toLowerCase().contains(newText.toLowerCase())
                || note.getNotes().toLowerCase().contains(newText.toLowerCase())){
                    filter.add(note);

                }
            }
            notesListAdapter.filterList(filter);
        }

    //pour recevoir les données envoyés pas le bouton d'enregistrement lors de l'ajout
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //si le code de la requete est de 101 ça signifie que c'est pour enregistrer les notes
        if(requestCode ==101){
            if(resultCode == Activity.RESULT_OK){ //alors on peut stocker la note dans notre BD
                Notes new_notes = (Notes) data.getSerializableExtra("note");
                database.mainDAO().insert(new_notes);
                notes.clear();
                notes.addAll(database.mainDAO().getAll());
                notesListAdapter.notifyDataSetChanged();

            }
        }
        //si le code de la requete est 102 ça signifie qu'on veut éditer une note
        else if(requestCode ==102){
            if(resultCode == Activity.RESULT_OK){
                Notes new_note = (Notes) data.getSerializableExtra("note");
                //on met à jour la base de données
                database.mainDAO().update(new_note.getId(),new_note.getTitle(),new_note.getNotes());
                notes.clear();
                notes.addAll(database.mainDAO().getAll());
                notesListAdapter.notifyDataSetChanged();

            }
        }
    }

    private void updateRecycler(List<Notes> notes) {
        //on met une taille fixe au recyclerView
        recyclerView.setHasFixedSize(true);
        //on définit le gestionnaire de disposition pour notre vue, pour les differentes tailles des elements contenant les notes
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(3, LinearLayoutManager.VERTICAL));
        //on initialise notre adaptateur de liste de notes
        notesListAdapter = new NotesListAdapter(MainActivity.this,notes,notesClickListener);
        recyclerView.setAdapter(notesListAdapter);
    }

    private final NotesClickListener notesClickListener = new NotesClickListener() {
        @Override
        public void onClick(Notes notes) {
            Intent intent = new Intent(MainActivity.this, NotesTakerActivity.class);
            //ajout des valeurs supplémentaires
            //chaque fois que l'utilisateur va cliquer sur un element
            //il va passer à la nouvelle activité pour éditer l'element
            intent.putExtra("old_version_note",notes); //les données sélectionnées sont transmises par "notes"
            startActivityForResult(intent,102); //pour éditer une note, le code de la requete demandé sera 102
        }

        @Override
        public void onLongClick(Notes notes, CardView cardView) {
            choosenNote = new Notes();
            choosenNote = notes;
            showPopup(cardView);

        }
    };

    private void showPopup(CardView cardView) {
        //on passe this et la vue sur laquelle on veut afficher le menu
        PopupMenu popup = new PopupMenu(this,cardView);
        popup.setOnMenuItemClickListener(this);
        popup.inflate(R.menu.popup_list);
        popup.show();
    }


    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()){
            case R.id.delete:
                database.mainDAO().delete(choosenNote);
                notes.clear();
                notes.addAll(database.mainDAO().getAll());
                notesListAdapter.notifyDataSetChanged();
                return true;
        }
        return false;
    }
}